FROM detochko/vscode-ssh-dev-node:1.2

RUN cd /root && \
    curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip" && \
    unzip awscli-bundle.zip && \
    sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

VOLUME /app /root /tmp

WORKDIR /app

CMD ["/usr/sbin/sshd", "-D"]